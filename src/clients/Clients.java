package clients;

public enum Clients {
    PostgreSQL, SQLite, OracleDB, MicrosoftSQLServer;

    public static Clients returnClient(String client){
        if(client.equals("PostgreSQL"))
            return PostgreSQL;
        if(client.equals("SQLite"))
            return SQLite;
        if(client.equals("Oracle DB"))
            return OracleDB;
        if(client.equals("Microsoft SQL Server"))
            return MicrosoftSQLServer;
        return null;
    }
}

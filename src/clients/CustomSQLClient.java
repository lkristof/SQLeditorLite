package clients;

import ai.DatabaseTables;
import ai.ForeignKey;
import ai.PrimaryKey;
import ai.UniqueKey;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

public class CustomSQLClient implements SQLClient {

    private Connection connection;
    private DatabaseTables tables = new DatabaseTables();
    private boolean tablesAvaliable = false;
    private List<ForeignKey> listOfForeignKeys = new ArrayList<>();
    private boolean foreignKeysAvaliable = false;
    private String tablesQuery;
    private String foreignKeysQuery;
    private boolean tablesQueryProvided = false;
    private List<PrimaryKey> listOfPrimaryKeys = new ArrayList<>();
    private HashSet<UniqueKey> listOfUniqueKeys = new HashSet<>();

    public CustomSQLClient(String connectionString, Properties props) throws SQLException {
        connection = DriverManager.getConnection(connectionString, props);
        refreshTablesAndConstraints();
    }

    public CustomSQLClient(String connectionString, Properties props, String tablesQuery, String foreignKeysQuery) throws SQLException {
        connection = DriverManager.getConnection(connectionString, props);
        this.tablesQuery = tablesQuery;
        this.foreignKeysQuery = foreignKeysQuery;
        tablesQueryProvided = true;
        refreshTablesAndConstraints();
    }

    @Override
    public Connection connect(String database, Properties props) throws SQLException {
        return null;
    }

    @Override
    public String parse(String query) throws SQLException {
        return query;
    }

    @Override
    public String getManualURL() {
        return "";
    }

    @Override
    public synchronized DatabaseTables getTables() {
        refreshTablesAndConstraints();
        return tables;
    }

    @Override
    public boolean tablesAvaliable() {
        return tablesAvaliable;
    }

    public Connection getConnection() {
        return connection;
    }

    public synchronized List<ForeignKey> getForeignKeys() {
        return listOfForeignKeys;
    }

    public synchronized boolean foreignKeysAvaliable() {
        return foreignKeysAvaliable;
    }

    public synchronized void refreshTablesAndConstraints() {
        tables = new DatabaseTables();
        if (tablesQueryProvided) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(tablesQuery);
                tablesAvaliable = true;
                while (resultSet.next()) {
                    tables.addTable(resultSet.getString(1));
                }
                extractTables(statement);
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                tablesAvaliable = false;
            }

            try {
                findKeys(foreignKeysQuery);
            } catch (Exception e) {
                foreignKeysAvaliable = false;
            }
        } else {
            try {
                DatabaseMetaData databaseMetaData = connection.getMetaData();
                ResultSet resultSet = databaseMetaData.getTables(null, null, null, new String[]{"TABLE"});
                while (resultSet.next()) {
                    tables.addTable(resultSet.getString("TABLE_NAME"));
                }
                extractTables(connection.createStatement());
                tablesAvaliable = true;
                findKeys("");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void findKeys(String foreignKeysQuery) throws SQLException {
        listOfForeignKeys.clear();
        if (tablesQueryProvided) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(foreignKeysQuery);
            while (resultSet.next()) {
                listOfForeignKeys.add(new ForeignKey(resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5)));
            }
            foreignKeysAvaliable = true;
        } else {
            ResultSet foreignKeysResultSet;
            for (String tableName : tables.getTables()) {
                foreignKeysResultSet = connection.getMetaData().getImportedKeys(null, null, tableName);
                while (foreignKeysResultSet.next()) {
                    listOfForeignKeys.add(new ForeignKey(foreignKeysResultSet.getString("fk_name"),
                            foreignKeysResultSet.getString("PKTABLE_NAME"),
                            foreignKeysResultSet.getString("PKCOLUMN_NAME"),
                            foreignKeysResultSet.getString("FKTABLE_NAME"),
                            foreignKeysResultSet.getString("FKCOLUMN_NAME")));
                }
            }
            foreignKeysAvaliable = true;
        }
        listOfPrimaryKeys.clear();
        listOfUniqueKeys.clear();
        for (String tableName : tables.getTables()) {
            ResultSet primaryKeysResultSet = connection.getMetaData().getPrimaryKeys(null, null, tableName);
            while (primaryKeysResultSet.next()) {
                listOfPrimaryKeys.add(new PrimaryKey(primaryKeysResultSet.getString("PK_NAME"),
                        primaryKeysResultSet.getString("COLUMN_NAME"),
                        tableName));
            }
            System.out.println(listOfPrimaryKeys.toString());
            ResultSet uniqueKeysResultSet = connection.getMetaData().getIndexInfo(null, null, tableName, true, false);
            while (uniqueKeysResultSet.next()) {
                listOfUniqueKeys.add(new UniqueKey("UK_" + uniqueKeysResultSet.getString("COLUMN_NAME"),
                        uniqueKeysResultSet.getString("TABLE_NAME"),
                        uniqueKeysResultSet.getString("COLUMN_NAME")));
            }
        }
        System.out.println(listOfUniqueKeys.toString());
    }

    private void extractTables(Statement statement) throws SQLException {
        if (tablesQueryProvided) {
            for (String tableName : tables.getTables()) {
                ResultSet resultSet = statement.executeQuery("Select * from " + tableName);
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 0; i < metaData.getColumnCount(); ++i) {
                    tables.addCollumn(tableName, metaData.getColumnName(i + 1));
                    System.out.println("-----------");
                    tables.addCollumn(tableName, metaData.getColumnName(i + 1), "");
                }
            }
        } else {
            for (String tableName : tables.getTables()) {
                ResultSet columnsResultSet = connection.getMetaData().getColumns(null, null, tableName, null);
                while (columnsResultSet.next()) {
                    tables.addCollumn(tableName, columnsResultSet.getString("COLUMN_NAME"));
                    tables.addCollumn(tableName, columnsResultSet.getString("COLUMN_NAME"), columnsResultSet.getString("TYPE_NAME"));
                }
            }

        }
    }

    @Override
    public List<PrimaryKey> getpriPrimaryKeys() {
        return listOfPrimaryKeys;
    }

    @Override
    public HashSet<UniqueKey> getUniqueKeys() {
        return listOfUniqueKeys;
    }
}

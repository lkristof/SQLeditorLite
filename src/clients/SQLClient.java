package clients;

import ai.DatabaseTables;
import ai.ForeignKey;
import ai.PrimaryKey;
import ai.UniqueKey;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

public interface SQLClient {
    Connection connect(String database, Properties props) throws SQLException;

    String parse(String query) throws SQLException;

    String getManualURL();

    DatabaseTables getTables();

    boolean tablesAvaliable();

    List<ForeignKey> getForeignKeys();

    boolean foreignKeysAvaliable();

    void refreshTablesAndConstraints();

    List<PrimaryKey> getpriPrimaryKeys();

    HashSet<UniqueKey> getUniqueKeys();
}

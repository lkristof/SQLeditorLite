package clients;

import ai.DatabaseTables;
import ai.ForeignKey;
import ai.PrimaryKey;
import ai.UniqueKey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

public class MicrosoftSQLServerSQLClient implements SQLClient {
    @Override
    public Connection connect(String database, Properties props) throws SQLException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String connectionUrl = "jdbc:sqlserver://localhost;database=" + database + ";";
        return DriverManager.getConnection(connectionUrl, props);
    }

    @Override
    public String parse(String query) {
        return query;
    }

    @Override
    public String getManualURL() {
        return "https://docs.microsoft.com/en-us/sql/?view=sql-server-2017";
    }

    @Override
    public DatabaseTables getTables() {
        return null;
    }

    @Override
    public boolean tablesAvaliable() {
        return false;
    }

    @Override
    public List<PrimaryKey> getpriPrimaryKeys() {
        return null;
    }

    @Override
    public List<ForeignKey> getForeignKeys() {
        return null;
    }

    @Override
    public boolean foreignKeysAvaliable() {
        return false;
    }

    @Override
    public synchronized void refreshTablesAndConstraints() {

    }

    @Override
    public HashSet<UniqueKey> getUniqueKeys() {
        return null;
    }
}

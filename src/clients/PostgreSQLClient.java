package clients;

import ai.DatabaseTables;
import ai.ForeignKey;
import ai.PrimaryKey;
import ai.UniqueKey;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

public class PostgreSQLClient implements SQLClient {

    private DatabaseTables tables = new DatabaseTables();
    private boolean tablesAvaliable = false;
    private Connection connection;
    private List<ForeignKey> listOfForeignKeys = new ArrayList<>();
    private List<PrimaryKey> listOfPrimaryKeys = new ArrayList<>();
    private HashSet<UniqueKey> listOfUniqueKeys = new HashSet<>();

    @Override
    public synchronized Connection connect(String database, Properties props) throws SQLException {
        String url = "jdbc:postgresql://localhost/" + database;
        Connection conn = DriverManager.getConnection(url, props);
        connection = conn;
        refreshTablesAndConstraints();
        return conn;
    }

    @Override
    public String parse(String query) throws SQLException {
        if (query.trim().isEmpty())
            throw new SQLException("QuerySense: Query is empty");
        if (query.split("\\'").length % 2 == 0 && query.split("\\'").length != 1 && !query.endsWith("\'"))
            throw new SQLException("QuerySense: \' not closed somewhere!");
        if (query.toLowerCase().trim().startsWith("select") && !query.toLowerCase().contains("from"))
            throw new SQLException("QuerySense: Select written without from, check spelling or write from [table_names]");
        return query;
    }

    @Override
    public String getManualURL() {
        return "https://www.postgresql.org/docs/manuals/";
    }

    @Override
    public synchronized DatabaseTables getTables() {
        return tables;
    }

    @Override
    public synchronized boolean tablesAvaliable() {
        return tablesAvaliable;
    }

    @Override
    public List<ForeignKey> getForeignKeys() {
        return listOfForeignKeys;
    }

    @Override
    public boolean foreignKeysAvaliable() {
        return true;
    }

    @Override
    public List<PrimaryKey> getpriPrimaryKeys() {
        return listOfPrimaryKeys;
    }

    @Override
    public synchronized void refreshTablesAndConstraints() {
        try {
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null, null, null, new String[] {"TABLE"});
            while(resultSet.next()){
                tables.addTable(resultSet.getString("TABLE_NAME"));
            }
            extractTables(connection.createStatement());
            tablesAvaliable = true;
            findKeys();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void findKeys() throws SQLException {
        listOfForeignKeys.clear();
        ResultSet foreignKeysResultSet;
        for(String tableName : tables.getTables()){
            foreignKeysResultSet = connection.getMetaData().getImportedKeys(null, null, tableName);
            while(foreignKeysResultSet.next()){
                listOfForeignKeys.add(new ForeignKey(foreignKeysResultSet.getString("fk_name"),
                        foreignKeysResultSet.getString("PKTABLE_NAME"),
                        foreignKeysResultSet.getString("PKCOLUMN_NAME"),
                        foreignKeysResultSet.getString("FKTABLE_NAME"),
                        foreignKeysResultSet.getString("FKCOLUMN_NAME")));
            }
        }
        listOfPrimaryKeys.clear();
        for(String tableName : tables.getTables()) {
            ResultSet primaryKeysResultSet = connection.getMetaData().getPrimaryKeys(null, null, tableName);
            while(primaryKeysResultSet.next()){
                listOfPrimaryKeys.add(new PrimaryKey(primaryKeysResultSet.getString("PK_NAME"),
                        primaryKeysResultSet.getString("COLUMN_NAME"),
                        tableName));
            }
            ResultSet uniqueKeysResultSet = connection.getMetaData().getIndexInfo(null, null, tableName, true, false);
            while (uniqueKeysResultSet.next()) {
                listOfUniqueKeys.add(new UniqueKey("UK_" + uniqueKeysResultSet.getString("COLUMN_NAME"),
                        uniqueKeysResultSet.getString("TABLE_NAME"),
                        uniqueKeysResultSet.getString("COLUMN_NAME")));
            }
        }
    }

    private void extractTables(Statement statement) throws SQLException {
        for(String tableName : tables.getTables()){
            ResultSet columnsResultSet = connection.getMetaData().getColumns(null, null, tableName, null);
            while(columnsResultSet.next()){
                tables.addCollumn(tableName, columnsResultSet.getString("COLUMN_NAME"));
                tables.addCollumn(tableName, columnsResultSet.getString("COLUMN_NAME"), columnsResultSet.getString("TYPE_NAME"));
            }
        }
    }

    @Override
    public HashSet<UniqueKey> getUniqueKeys() {
        return listOfUniqueKeys;
    }
}

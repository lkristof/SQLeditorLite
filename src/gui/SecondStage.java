package gui;

import ai.ForeignKey;
import ai.PrimaryKey;
import ai.PseudoTable;
import ai.UniqueKey;
import clients.*;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.List;

public class SecondStage {
    private SQLClient clientForBase;
    private Connection conn = null;
    private TableView<ObservableList<String>> tableView = new TableView<>();
    private JTextPane textPane = new JTextPane();
    private Map<Integer, List<String>> mapOfQuery = new HashMap<>();
    private BorderPane borderPane;
    private TreeView<String> tablesView;
    private HBox boxForTablesView;
    private UndoManager undoStack = new UndoManager();
    private VBox boxForQueryField = new VBox();
    private String dateFormatString = "dd-mm-yyyy";
    private boolean firstTimeSpace = true;
    private double spaceValue = 0;

    public SecondStage(String user, String pass, String database, Clients client, Stage secondaryStage) throws SQLException {
        //Connecting to database----------------------------------------------------------------
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pass);
        if (client.equals(Clients.PostgreSQL)) {
            clientForBase = new PostgreSQLClient();
            conn = clientForBase.connect(database, props);
        }
        if (client.equals(Clients.MicrosoftSQLServer)) {
            clientForBase = new MicrosoftSQLServerSQLClient();
            conn = clientForBase.connect(database, props);
        }
        initGUI(secondaryStage);
    }

    public SecondStage(CustomSQLClient client, Stage secondaryStage) {
        this.clientForBase = client;
        this.conn = client.getConnection();
        initGUI(secondaryStage);
    }

    private void initGUI(Stage secondaryStage) {
        //Design of a stage------------------------------------------------------------------------
        borderPane = new BorderPane();
        borderPane.getStyleClass().add("borderPane");
        HBox boxForTop = new HBox();
        borderPane.setTop(boxForTop);

        Menu fileMenu = new Menu("File");
        MenuItem itemOpen = new MenuItem("Open SQL script");
        itemOpen.setOnAction((e) -> openSQLScript(secondaryStage));
        MenuItem itemSave = new MenuItem("Save SQL script");
        itemSave.setOnAction((e) -> saveSQLScript(secondaryStage));
        MenuItem itemClose = new MenuItem("Exit");
        itemClose.setOnAction((eve) -> secondaryStage.close());
        fileMenu.getItems().add(itemOpen);
        fileMenu.getItems().add(itemSave);
        MenuBar fileBar = new MenuBar(fileMenu);

        Menu editMenu = new Menu("Edit");
        MenuItem itemUndo = new MenuItem("Undo (ctrl + z)");
        MenuItem itemRedo = new MenuItem("Redo (ctrl + y)");
        MenuItem itemCut = new MenuItem("Cut (ctrl + x)");
        MenuItem itemCopy = new MenuItem("Copy (ctrl + c)");
        MenuItem itemPaste = new MenuItem("Paste (ctrl + v)");
        MenuItem itemSelectAll = new MenuItem("Select all");
        itemUndo.setOnAction((ActionEvent e) -> {
            undoStack.undo();
            undoStack.undo();
        });
        itemRedo.setOnAction((ActionEvent e) -> {
            undoStack.redo();
            undoStack.redo();
        });
        itemCut.setOnAction((e) -> textPane.cut());
        itemCopy.setOnAction((e) -> textPane.copy());
        itemPaste.setOnAction((e) -> textPane.paste());
        itemSelectAll.setOnAction((a) -> {
            textPane.setSelectionStart(0);
            textPane.setSelectionEnd(textPane.getText().length());
        });
        MenuItem itemClearQuery = new MenuItem("Clear query");
        itemClearQuery.setOnAction((ev) -> textPane.setText(""));
        editMenu.getItems().add(itemUndo);
        editMenu.getItems().add(itemRedo);
        editMenu.getItems().add(itemCut);
        editMenu.getItems().add(itemCopy);
        editMenu.getItems().add(itemPaste);
        editMenu.getItems().add(itemSelectAll);
        editMenu.getItems().add(itemClearQuery);
        MenuBar editBar = new MenuBar(editMenu);

        Menu connectionMenu = new Menu("Connection");
        MenuItem connectToAnotherDatabaseItem = new MenuItem("Connect to another database");
        MenuItem connectionInfoItem = new MenuItem("Info for current connection");
        connectToAnotherDatabaseItem.setOnAction((even) -> connectToAnotherDatabase());
        connectionInfoItem.setOnAction((eve) -> showInfoAboutConnection());
        connectionMenu.getItems().add(connectToAnotherDatabaseItem);
        connectionMenu.getItems().add(connectionInfoItem);
        MenuBar connectionBar = new MenuBar(connectionMenu);

        Menu queryMenu = new Menu("Query");
        MenuItem executeItem = new MenuItem("Execute query");
        MenuItem generateSelectItem = new MenuItem("Generate select statement");
        MenuItem deleteTableItem = new MenuItem("Delete table");
        generateSelectItem.setOnAction((e) -> startSelectStage());
        deleteTableItem.setOnAction((ev) -> startDeleteTableStage());
        queryMenu.getItems().add(executeItem);
        queryMenu.getItems().add(generateSelectItem);
        queryMenu.getItems().add(deleteTableItem);
        MenuBar queryBar = new MenuBar(queryMenu);

        SwingNode swingNode = new SwingNode();

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        ScrollPane s1 = new ScrollPane();
        s1.setPrefSize(400, 350);
        DragResizeMod.makeResizable(s1, null);
        textPane.setPreferredSize(new Dimension((int) screenBounds.getWidth(), (int) screenBounds.getHeight()));
        textPane.setFont(new Font("Verdana", Font.PLAIN, 16));
        JScrollPane scrollPane = new JScrollPane(textPane);
        scrollPane.setPreferredSize(new Dimension((int) screenBounds.getWidth(), (int) screenBounds.getHeight()));
        createAndSetSwingContent(swingNode, scrollPane);
        s1.setContent(swingNode);
        boxForTop.getChildren().add(fileBar);
        boxForTop.getChildren().add(editBar);
        boxForTop.getChildren().add(connectionBar);
        boxForTop.getChildren().add(queryBar);

        boxForQueryField.getChildren().add(s1);
        borderPane.setLeft(boxForQueryField);

        s1.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(KeyCode.SPACE)) {
                if(firstTimeSpace) {
                    spaceValue = s1.getVvalue();
                    firstTimeSpace = false;
                }
                System.out.println(s1.getVvalue());
                s1.setVvalue(s1.getVvalue() - spaceValue);
            }
        });

        Menu menuSave = new Menu("Save");
        MenuItem itemSaveAsCSV = new MenuItem("Save as CSV");
        MenuItem itemSaveAsExcel = new MenuItem("Save as Excel");
        menuSave.getItems().add(itemSaveAsCSV);
        menuSave.getItems().add(itemSaveAsExcel);
        itemSaveAsCSV.setOnAction((ev) -> saveAsCSV(secondaryStage));
        itemSaveAsExcel.setOnAction((eve) -> saveAsExcel(secondaryStage));
        fileMenu.getItems().add(menuSave);
        fileMenu.getItems().add(itemClose);

        Menu menuView = new Menu("View");
        MenuItem refreshTablesItem = new MenuItem("Refresh tables and constraints");
        MenuItem fullScreenItem = new MenuItem("Full screen");
        fullScreenItem.setOnAction((eve) -> {
            if (secondaryStage.isFullScreen())
                secondaryStage.setFullScreen(false);
            else
                secondaryStage.setFullScreen(true);
        });
        MenuItem backgroundItem = new MenuItem("Change background");
        backgroundItem.setOnAction((eve) -> changeBackground(secondaryStage));
        MenuItem dateFormatItem = new MenuItem("Set date format");
        dateFormatItem.setOnAction((even) -> changeDateFormat());
        menuView.getItems().add(dateFormatItem);
        menuView.getItems().add(backgroundItem);
        menuView.getItems().add(refreshTablesItem);
        menuView.getItems().add(fullScreenItem);
        MenuBar viewBar = new MenuBar(menuView);
        boxForTop.getChildren().add(viewBar);

        Menu menuHelp = new Menu("Help");
        MenuItem manualHelpItem = new MenuItem("Help manual");
        manualHelpItem.setOnAction((ActionEvent e) -> startHelpStage());
        MenuItem onlineHelpManualItem = new MenuItem("Online help for DBMS");
        onlineHelpManualItem.setOnAction((even) -> {
            try {
                URI uri = new URI(clientForBase.getManualURL());
                Desktop desktop = Desktop.getDesktop();
                desktop.browse(uri);
            } catch (Exception e) {
                handleException(e);
            }
        });
        menuHelp.getItems().add(manualHelpItem);
        menuHelp.getItems().add(onlineHelpManualItem);
        MenuBar helpBar = new MenuBar(menuHelp);
        boxForTop.getChildren().add(helpBar);

        tablesView = generateTreeView();
        tablesView.setPrefSize(210, 410);
        tablesView.setMaxSize(210, 410);
        tablesView.setMinSize(210, 410);
        boxForTablesView = new HBox(tablesView);
        boxForTablesView.setAlignment(Pos.TOP_RIGHT);
        HBox.setHgrow(boxForTablesView, Priority.ALWAYS);
        borderPane.setRight(boxForTablesView);

        refreshTablesItem.setOnAction((e) -> {
            tablesView = generateTreeView();
            tablesView.setPrefSize(210, 410);
            tablesView.setMaxSize(210, 410);
            tablesView.setMinSize(210, 410);
            boxForTablesView = new HBox(tablesView);
            boxForTablesView.setAlignment(Pos.TOP_RIGHT);
            HBox.setHgrow(boxForTablesView, Priority.ALWAYS);
            borderPane.setRight(boxForTablesView);
        });

        tableView.setPrefHeight(250);

        borderPane.setBottom(tableView);

        s1.setMaxWidth(1300);

        //Text painting-----------------------------------------------------------

        DefaultStyledDocument doc = new StyleForText();

        textPane.setStyledDocument(doc);

        //Query execution------------------------------------------------------------------------------------
        executeItem.setOnAction((e) -> executeQuery());

        //Changes listener-------------------------------------------------------
        Document document = textPane.getDocument();
        document.addUndoableEditListener(evt -> undoStack.addEdit(evt.getEdit()));
        textPane.getActionMap().put("Undo", new AbstractAction("Undo") {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    if (undoStack.canUndo()) {
                        undoStack.undo();
                        undoStack.undo();
                    }
                } catch (CannotUndoException e1) {
                    handleUndoException(e1);
                }
            }
        });
        textPane.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
        textPane.getActionMap().put("Redo", new AbstractAction("Redo") {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    if (undoStack.canRedo()) {
                        undoStack.redo();
                        undoStack.redo();
                    }
                } catch (CannotRedoException e1) {
                    handleUndoException(e1);
                }
            }
        });
        textPane.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
        borderPane.setOnKeyPressed((eve) -> {
            if (eve.isControlDown())
                if (eve.getCode().equals(KeyCode.R))
                    executeQuery();
        });
    }

    private void showInfoAboutConnection() {
        Stage infoStage = new Stage();
        javafx.scene.control.TextArea areaForInfo = new javafx.scene.control.TextArea();
        areaForInfo.setEditable(false);
        ScrollPane stackPane = new ScrollPane(areaForInfo);
        areaForInfo.setText("");
        try {
            areaForInfo.appendText("Database product name: " + conn.getMetaData().getDatabaseProductName() + "\n");
            areaForInfo.appendText("Database product version: " + conn.getMetaData().getDatabaseProductVersion() + "\n");
            areaForInfo.appendText("Driver name: " + conn.getMetaData().getDriverName() + "\n");
            areaForInfo.appendText("Driver version: " + conn.getMetaData().getDriverVersion() + "\n");
            areaForInfo.appendText("Numeric functions: " + conn.getMetaData().getNumericFunctions() + "\n");
            areaForInfo.appendText("String functions: " + conn.getMetaData().getStringFunctions() + "\n");
            areaForInfo.appendText("System functions: " + conn.getMetaData().getSystemFunctions() + "\n");
            areaForInfo.appendText("TimeDate functions: " + conn.getMetaData().getTimeDateFunctions() + "\n");
            areaForInfo.appendText("URL: " + conn.getMetaData().getURL() + "\n");
            areaForInfo.appendText("User: " + conn.getMetaData().getUserName() + "\n");
        } catch (SQLException e) {
            areaForInfo.setText("Data retrieve failed");
        }

        Scene scene = new Scene(stackPane);
        infoStage.setScene(scene);
        infoStage.show();
    }

    private void saveAsExcel(Stage secondaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Set XLSX filename and position");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        File file = fileChooser.showSaveDialog(secondaryStage);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("table");
        XSSFRow row;
        for (int i = 0; i < mapOfQuery.size(); ++i) {
            row = spreadsheet.createRow(i);
            int j = 0;
            for (String value : mapOfQuery.get(i)) {
                XSSFCell cell = row.createCell(j);
                cell.setCellValue(value);
                j++;
            }
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            handleException(e);
        }
        try {
            workbook.write(new FileOutputStream(file));
        } catch (IOException e) {
            handleException(e);
        }
    }

    private void connectToAnotherDatabase() {
        Stage connectStage = new Stage();
        connectStage.setTitle("Connect");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Connect");
        scenetitle.setFont(javafx.scene.text.Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Label database = new Label("Database:");
        grid.add(database, 0, 3);

        TextField databaseTextField = new TextField();
        grid.add(databaseTextField, 1, 3);

        Label clientLabel = new Label("SQL client:");
        grid.add(clientLabel, 0, 4);

        ComboBox clientComboBox = new ComboBox(getOptionsForDBClient());
        grid.add(clientComboBox, 1, 4);

        Button buttonForSignIn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(buttonForSignIn);
        grid.add(hbBtn, 1, 6);

        final Text actiontarget = new Text();

        Button buttonForLoadConfig = new Button("Load configuration file");
        HBox hbBtn2 = new HBox(10);
        hbBtn2.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn2.getChildren().add(buttonForLoadConfig);
        grid.add(hbBtn2, 0, 6);
        buttonForLoadConfig.setOnAction((e) -> {
            try {
                loadConfigFile(connectStage);
                connectStage.close();
            } catch (Exception e1) {
                actiontarget.setText("JSON file not found or invalid");
                actiontarget.setFill(Color.FIREBRICK);
            }
        });

        grid.add(actiontarget, 1, 7);

        Scene scene = new Scene(grid, 400, 375);

        connectStage.setResizable(false);

        buttonForSignIn.setOnAction((e) -> {
            actiontarget.setText("");
            if (userTextField.getText().isEmpty()) {
                actiontarget.setText("Username field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (pwBox.getText().isEmpty()) {
                actiontarget.setText("Password field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (databaseTextField.getText().isEmpty()) {
                actiontarget.setText("Database field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (clientComboBox.getValue() == null) {
                actiontarget.setText("Client must be chosen");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            String client = (String) clientComboBox.getValue();
            try {
                Properties props = new Properties();
                props.setProperty("user", userTextField.getText());
                props.setProperty("password", pwBox.getText());
                if (Objects.equals(Clients.returnClient(client), Clients.PostgreSQL)) {
                    clientForBase = new PostgreSQLClient();
                    conn = clientForBase.connect(databaseTextField.getText(), props);
                }
                if (Objects.equals(Clients.returnClient(client), Clients.MicrosoftSQLServer)) {
                    clientForBase = new MicrosoftSQLServerSQLClient();
                    conn = clientForBase.connect(databaseTextField.getText(), props);
                }
                clientForBase.refreshTablesAndConstraints();
                tablesView = generateTreeView();
                tablesView.setPrefSize(210, 410);
                tablesView.setMaxSize(210, 410);
                tablesView.setMinSize(210, 410);
                boxForTablesView = new HBox(tablesView);
                boxForTablesView.setAlignment(Pos.TOP_RIGHT);
                HBox.setHgrow(boxForTablesView, Priority.ALWAYS);
                borderPane.setRight(boxForTablesView);
                connectStage.close();
            } catch (SQLException e1) {
                actiontarget.setText(e1.getMessage().replace("FATAL:", ""));
                actiontarget.setFill(Color.FIREBRICK);
                System.err.println(e1.getMessage());
                connectStage.setResizable(true);
            }
        });

        scene.setOnKeyPressed((e) -> {
            if (e.getCode() == KeyCode.ENTER) {
                buttonForSignIn.fire();
            }
        });
        scene.getStylesheets().add
                (Main.class.getResource("loginSecond.css").toExternalForm());
        connectStage.setScene(scene);
        connectStage.show();
    }

    private void loadConfigFile(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        Path path = fileChooser.showOpenDialog(primaryStage).toPath();

        Task<CustomSQLClient> parseJSONAndConnectTask = new Task<CustomSQLClient>() {
            @Override
            protected CustomSQLClient call() throws Exception {
                String connectionString = "";
                Properties props = new Properties();
                String tablesQuery = "";
                String foreignKeysQuery = "";
                boolean tablesQueryProvided = false;

                StringBuilder jsonFile = new StringBuilder();
                for (String line : Files.readAllLines(path))
                    jsonFile.append(line).append("\n");
                JSONObject jsonObject = new JSONObject(jsonFile.toString());
                JSONObject databaseSettingsObject = jsonObject.getJSONObject("databaseSettings");
                for (String key : databaseSettingsObject.keySet()) {
                    if (key.equals("connectionString"))
                        connectionString = databaseSettingsObject.getString(key);
                    if (key.equals("tablesQuery")) {
                        tablesQuery = databaseSettingsObject.getString(key);
                        tablesQueryProvided = true;
                    }
                    if (key.equals("foreignKeysQuery"))
                        foreignKeysQuery = tablesQuery = databaseSettingsObject.getString(key);
                    else
                        props.setProperty(key, databaseSettingsObject.getString(key));
                }
                if (tablesQueryProvided) {
                    CustomSQLClient client = new CustomSQLClient(connectionString, props, tablesQuery, foreignKeysQuery);
                    clientForBase = client;
                    conn = client.getConnection();
                    return new CustomSQLClient(connectionString, props, tablesQuery, foreignKeysQuery);
                } else {
                    CustomSQLClient client = new CustomSQLClient(connectionString, props);
                    clientForBase = client;
                    conn = client.getConnection();
                    System.out.println("CLIENT");
                    return new CustomSQLClient(connectionString, props);
                }
            }
        };
        parseJSONAndConnectTask.setOnSucceeded((e) -> {
            clientForBase.refreshTablesAndConstraints();
            tablesView = generateTreeView();
            tablesView.setPrefSize(210, 410);
            tablesView.setMaxSize(210, 410);
            tablesView.setMinSize(210, 410);
            boxForTablesView = new HBox(tablesView);
            boxForTablesView.setAlignment(Pos.TOP_RIGHT);
            HBox.setHgrow(boxForTablesView, Priority.ALWAYS);
            borderPane.setRight(boxForTablesView);
        });
        parseJSONAndConnectTask.setOnFailed((e) -> System.out.println(e.getSource().getException()));
        new Thread(parseJSONAndConnectTask).start();
    }

    private void executeQuery() {
        if (boxForQueryField.getChildren().size() > 1)
            boxForQueryField.getChildren().remove(1);
        Task<ResultSet> execQueryTask = new Task<ResultSet>() {
            @Override
            protected ResultSet call() throws SQLException {
                return parseAndExecute(textPane.getText(), clientForBase, conn);
            }
        };

        execQueryTask.setOnSucceeded(s -> {
            ResultSet result = execQueryTask.getValue();
            try {
                if (result.isBeforeFirst())
                    mapOfQuery.clear();
            } catch (SQLException exc) {
                return;
            }
            ResultSetMetaData resultSetMetaData;
            tableView = new TableView<>();
            mapOfQuery.put(0, new ArrayList<>());
            try {
                resultSetMetaData = result.getMetaData();
                for (int i = 0; i < resultSetMetaData.getColumnCount(); ++i) {
                    final int finalIdx = i;
                    TableColumn<ObservableList<String>, String> column = new TableColumn<>(
                            resultSetMetaData.getColumnName(i + 1)
                    );
                    column.setCellValueFactory(param ->
                            new ReadOnlyObjectWrapper<>(param.getValue().get(finalIdx))
                    );
                    tableView.getColumns().add(column);
                    mapOfQuery.get(0).add(resultSetMetaData.getColumnName(i + 1));
                }

                int j = 1;
                while (result.next()) {
                    ArrayList<String> itemsInRow = new ArrayList<>();
                    mapOfQuery.put(j, new ArrayList<>());
                    for (int i = 0; i < resultSetMetaData.getColumnCount(); ++i) {
                        try {
                            Date resulta = result.getDate(i + 1);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(resulta);
                            String dateToAdd = dateFormatString.replace("yyyy", Integer.toString(cal.get(Calendar.YEAR)))
                                    .replace("mm", Integer.toString(cal.get(Calendar.MONTH)))
                                    .replace("dd", Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
                            itemsInRow.add(dateToAdd);
                            mapOfQuery.get(j).add(dateToAdd);
                            System.out.println(dateToAdd);
                        } catch (Exception exc) {
                            itemsInRow.add(result.getString(i + 1));
                            mapOfQuery.get(j).add(result.getString(i + 1));
                        }

                    }

                    tableView.getItems().add(
                            FXCollections.observableArrayList(
                                    itemsInRow
                            )
                    );
                    j++;
                }
                tableView.setPrefHeight(250);

                borderPane.setBottom(tableView);
            } catch (SQLException e1) {
                handleException(e1);
            }

        });
        execQueryTask.setOnFailed(er -> handleException(er.getSource().getException()));
        new Thread(execQueryTask).start();
    }

    private void changeDateFormat() {
        Stage changeDateFormatStage = new Stage();
        changeDateFormatStage.setTitle("Change date format");
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(gridPane);

        Label defineDateFormatlabel = new Label("Define date format: ");
        gridPane.add(defineDateFormatlabel, 0, 0);
        TextField dateFormatTextField = new TextField();
        gridPane.add(dateFormatTextField, 1, 0);
        Button changeDateFormatButton = new Button("Change date format");
        gridPane.add(changeDateFormatButton, 1, 1);
        changeDateFormatButton.setOnAction((eve) -> {
            if (!dateFormatTextField.getText().contains("dd")) {
                Text actiontarget = new Text();
                actiontarget.setText("Format must contain dd!");
                actiontarget.setFill(Color.FIREBRICK);
                gridPane.add(actiontarget, 1, 2);
            } else if (!dateFormatTextField.getText().contains("mm")) {
                Text actiontarget = new Text();
                actiontarget.setText("Format must contain mm!");
                actiontarget.setFill(Color.FIREBRICK);
                gridPane.add(actiontarget, 1, 2);
            } else if (!dateFormatTextField.getText().contains("yyyy")) {
                Text actiontarget = new Text();
                actiontarget.setText("Format must contain yyyy!");
                actiontarget.setFill(Color.FIREBRICK);
                gridPane.add(actiontarget, 1, 2);
            } else {
                dateFormatString = dateFormatTextField.getText();
                changeDateFormatStage.close();
            }
        });
        changeDateFormatStage.setScene(scene);
        changeDateFormatStage.show();
    }

    private void startDeleteTableStage() {
        clientForBase.refreshTablesAndConstraints();
        Stage deleteStage = new Stage();
        deleteStage.setTitle("Delete table");
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(gridPane);

        Label labelDelete = new Label("Choose a table: ");
        gridPane.add(labelDelete, 0, 0);

        ComboBox<String> comboBoxTables = new ComboBox<>();
        comboBoxTables.getItems().addAll(clientForBase.getTables().getTables());
        gridPane.add(comboBoxTables, 1, 0);

        Button deleteButton = new Button("Delete table");
        deleteButton.setOnAction((even) -> {
            try {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm table delete");
                alert.setContentText("Are you sure you want to delete table " + comboBoxTables.getValue() + "?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent() && result.get() == ButtonType.OK) {
                    parseAndExecute("drop table " + comboBoxTables.getValue(), clientForBase, conn);
                }
            } catch (SQLException ignored) {
            }
        });
        gridPane.add(deleteButton, 1, 1);

        deleteStage.setScene(scene);
        deleteStage.show();
    }

    private void saveSQLScript(Stage secondaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose script name and location");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        File file = fileChooser.showSaveDialog(secondaryStage);
        try {
            FileWriter writer = new FileWriter(file.getAbsolutePath() + ".sql");
            writer.write(textPane.getText());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            handleException(e);
        }
    }

    private void openSQLScript(Stage secondaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose script");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        File file = fileChooser.showOpenDialog(secondaryStage);
        StringBuilder lines = new StringBuilder();
        try {
            for (String line : Files.readAllLines(file.toPath())) {
                lines.append(line).append("\n");
            }
            textPane.setText(lines.toString());
        } catch (IOException e) {
            handleException(e);
        }
    }

    private void changeBackground(Stage secondaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose background");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        File file = fileChooser.showOpenDialog(secondaryStage);
        borderPane.setBackground(new Background(new BackgroundImage(new Image("file:\\" + file.getAbsolutePath()), null, null, null, null)));
    }

    private void handleUndoException(Exception e1) {
        System.out.println(e1.getMessage());
    }

    private void startHelpStage() {
        Stage helpStage = new Stage();
        BorderPane helpPane = new BorderPane();
        WebView webView = new WebView();
        URL url = this.getClass().getResource("Guide.htm");
        webView.getEngine().load(url.toString());
        helpPane.setCenter(webView);
        Scene helpScene = new Scene(helpPane, 400, 350);
        helpStage.setScene(helpScene);
        helpStage.show();
    }

    private void saveAsCSV(Stage secondaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Set CSV filename and position");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        File file = fileChooser.showSaveDialog(secondaryStage);
        FileWriter writer;
        try {
            writer = new FileWriter(new File(file.getAbsolutePath() + ".csv"));
            for (int i = 0; i < mapOfQuery.size(); ++i) {
                for (String value : mapOfQuery.get(i))
                    writer.write(value + ",");
                writer.write("\n");
            }
            writer.flush();
        } catch (IOException | NullPointerException e1) {
            handleException(e1);
        }

    }

    private void startSelectStage() {
        Stage selectStage = new Stage();
        selectStage.setTitle("Generate select");
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));

        javafx.scene.control.Label selectLabel = new javafx.scene.control.Label("What to select: ");
        gridPane.add(selectLabel, 0, 0);
        javafx.scene.control.TextArea selectText = new javafx.scene.control.TextArea();
        selectText.setPrefHeight(15);
        gridPane.add(selectText, 1, 0);

        javafx.scene.control.Label fromLabel = new javafx.scene.control.Label("Where to select from: ");
        gridPane.add(fromLabel, 0, 1);
        javafx.scene.control.TextArea fromText = new javafx.scene.control.TextArea();
        fromText.setPrefHeight(15);
        gridPane.add(fromText, 1, 1);

        javafx.scene.control.Label whereLabel = new javafx.scene.control.Label("Where clause: ");
        gridPane.add(whereLabel, 0, 2);
        javafx.scene.control.TextArea whereText = new javafx.scene.control.TextArea();
        whereText.setPrefHeight(15);
        gridPane.add(whereText, 1, 2);

        javafx.scene.control.Label groupByLabel = new javafx.scene.control.Label("Group by: ");
        gridPane.add(groupByLabel, 0, 3);
        javafx.scene.control.TextArea groupByText = new javafx.scene.control.TextArea();
        groupByText.setPrefHeight(15);
        gridPane.add(groupByText, 1, 3);

        javafx.scene.control.Label orderByLabel = new javafx.scene.control.Label("Order by: ");
        gridPane.add(orderByLabel, 0, 4);
        javafx.scene.control.TextArea orderByText = new javafx.scene.control.TextArea();
        orderByText.setPrefHeight(15);
        gridPane.add(orderByText, 1, 4);

        Button generateButton = new Button("Generate statement");
        gridPane.add(generateButton, 1, 5);

        CheckBox joinCheckBox = new CheckBox("Join tables");
        gridPane.add(joinCheckBox, 0, 5);

        Text errorText = new Text();
        gridPane.add(errorText, 1, 6);

        generateButton.setOnAction((event) -> {
            try {
                String query = generateSelectStatement(selectText.getText(), fromText.getText(), whereText.getText(),
                        orderByText.getText(), groupByText.getText(), joinCheckBox.isSelected());
                textPane.setText(query);
            } catch (SQLException exc) {
                errorText.setText(exc.getMessage());
            }
        });

        ScrollPane scrollPane = new ScrollPane(gridPane);

        Scene scene = new Scene(scrollPane);
        selectStage.setScene(scene);
        selectStage.show();
    }

    private String generateSelectStatement(String what, String whereFrom, String whereClause, String orderBy, String groupBy, boolean joinChecked) throws SQLException {
        StringBuilder catString = new StringBuilder();
        List<PseudoTable> listOfTablesAndPseudos = new ArrayList<>();
        if (what.trim().isEmpty())
            throw new SQLException("You must select something. What to select can not be empty");
        if (whereFrom.trim().isEmpty())
            throw new SQLException("You must select from somewhere. Where from to select can not be empty");
        if (clientForBase.tablesAvaliable()) {
            String[] whereFromArray = whereFrom.split(",");
            System.out.println(clientForBase.getTables().getTables().toString());
            for (String table : whereFromArray) {
                System.out.println(table);
                String[] tableArray = table.trim().split("\\W+");
                System.out.println(tableArray.length);
                if (!(tableArray.length == 1 || tableArray.length == 3))
                    throw new SQLException("Error at " + table + " in where to select from clause. HINT - there are to many words");
                if (!clientForBase.getTables().getTables().contains(tableArray[0].trim()))
                    throw new SQLException("Database does not contain table " + table + " named in FROM clause");
                if (tableArray.length == 1)
                    listOfTablesAndPseudos.add(new PseudoTable(tableArray[0], tableArray[0]));
                if (tableArray.length == 3) {
                    try {
                        Double.parseDouble(tableArray[2]);
                        throw new SQLException("Alias " + tableArray[2] + " can't be a number.");
                    } catch (NumberFormatException e) {
                        listOfTablesAndPseudos.add(new PseudoTable(tableArray[0], tableArray[2]));
                    }
                }
            }
            String[] whatToSelectArray = what.split(",");
            for (String whatData : whatToSelectArray) {
                if (whatData.trim().equals("*") && whatToSelectArray.length == 1)
                    continue;
                if (whatData.trim().equals("*") && whatToSelectArray.length != 1)
                    throw new SQLException("Wrong usage of * operator");
                if (whatData.contains("(") && whatData.contains(")"))
                    continue;
                String[] arrayOfOneWhat = whatData.split("\\.");
                if (!(arrayOfOneWhat.length == 1 || arrayOfOneWhat.length == 2))
                    throw new SQLException("Error in what part: Database does not contain " + what + ". \nHint: too many . delimiters");
                if (arrayOfOneWhat.length == 1) {
                    String[] arrOfPseudos = arrayOfOneWhat[0].trim().split("\\W+");
                    if (!(arrOfPseudos.length == 1 || arrOfPseudos.length == 3))
                        throw new SQLException("Error: too many words on: " + arrayOfOneWhat[0]);
                    if (arrOfPseudos.length == 3) {
                        if (!arrOfPseudos[1].trim().toLowerCase().equals("as"))
                            throw new SQLException("Error: wrong as clause on: " + arrayOfOneWhat[0]);
                        else arrayOfOneWhat[0] = arrOfPseudos[0].trim();
                    }
                    boolean containsAttribute = false;
                    for (PseudoTable pseudoTable : listOfTablesAndPseudos) {
                        if (clientForBase.getTables().getCollumns(pseudoTable.getTableName().trim()).contains(arrayOfOneWhat[0].trim()))
                            containsAttribute = true;
                    }
                    if (!containsAttribute)
                        throw new SQLException("Unknown attribute " + arrayOfOneWhat[0].trim());
                }
                if (arrayOfOneWhat.length == 2) {
                    for (PseudoTable pseudoTable : listOfTablesAndPseudos)
                        if (arrayOfOneWhat[0].trim().equals(pseudoTable.getTableAlias().trim()))
                            arrayOfOneWhat[0] = pseudoTable.getTableName().trim();
                    if (!clientForBase.getTables().getTables().contains(arrayOfOneWhat[0].trim()))
                        throw new SQLException("Error in what part: Database does not contain table: " + arrayOfOneWhat[0].trim());
                    if (!clientForBase.getTables().getCollumns(arrayOfOneWhat[0].trim()).contains(arrayOfOneWhat[1].trim().split("\\W")[0]) && !arrayOfOneWhat[1].trim().split("\\W")[0].equals("*"))
                        throw new SQLException("Error in what part: Database does not contain table " + arrayOfOneWhat[0].trim() + " with column " + arrayOfOneWhat[1].trim());
                    String[] checkArray = arrayOfOneWhat[1].trim().split("\\W");
                    if (!(checkArray.length == 1 || checkArray.length == 3))
                        throw new SQLException("Improper format of " + arrayOfOneWhat[1].trim() + " in what to select part");
                    if (checkArray.length == 3) {
                        if (!checkArray[1].trim().toLowerCase().equals("as"))
                            throw new SQLException("Improper format of " + arrayOfOneWhat[1].trim() + " in what to select part");
                    }
                }
            }
            if (joinChecked) {
                List<String> joins = new ArrayList<>();
                for (PseudoTable firstTable : listOfTablesAndPseudos) {
                    for (PseudoTable secondTable : listOfTablesAndPseudos) {
                        for (ForeignKey foreignKey : clientForBase.getForeignKeys()) //Check if there's a foreign key
                            if (foreignKey.getTableName().trim().equals(secondTable.getTableName().trim()) &&
                                    foreignKey.getForeignTableName().trim().equals(firstTable.getTableName().trim()) &&
                                    !firstTable.equals(secondTable)) {
                                if (firstTable.getTableName().trim().equals(secondTable.getTableName().trim())) {
                                    System.out.println(listOfTablesAndPseudos.indexOf(firstTable));
                                    if (listOfTablesAndPseudos.indexOf(firstTable) <= listOfTablesAndPseudos.indexOf(secondTable))
                                        continue;
                                }
                                System.out.print(firstTable.getTableAlias());
                                System.out.println(" ------------------------- " + secondTable.getTableAlias());
                                boolean flag = false;
                                for (PseudoTable tableRecon : listOfTablesAndPseudos)
                                    if (listOfTablesAndPseudos.indexOf(tableRecon) > listOfTablesAndPseudos.indexOf(secondTable) && !(listOfTablesAndPseudos.indexOf(tableRecon) >= listOfTablesAndPseudos.indexOf(firstTable))) {
                                        flag = true;
                                        break;
                                    }
                                if (flag)
                                    continue;
                                if (firstTable.getTableName().trim().equals(secondTable.getTableName().trim()))
                                    joins.add(firstTable.getTableAlias().trim() + "." + foreignKey.getColumnName() + "=" + secondTable.getTableAlias().trim() + "." + foreignKey.getForeignColumnName());
                                else
                                    joins.add(secondTable.getTableAlias().trim() + "." + foreignKey.getColumnName() + "=" + firstTable.getTableAlias().trim() + "." + foreignKey.getForeignColumnName());
                            }
                    }
                }
                System.out.println(joins.toString());
                System.out.println(listOfTablesAndPseudos.toString());
                if (!whereClause.trim().isEmpty())
                    catString.append(" \nWHERE ").append(whereClause);
                if (whereClause.trim().isEmpty() && !joins.isEmpty())
                    catString.append(" \nWHERE ").append(joins.get(0));
                if (!joins.isEmpty()) {
                    if (!whereClause.trim().isEmpty())
                        catString.append(" \nAND ").append(joins.get(0));
                    for (int i = 1; i < joins.size(); ++i)
                        catString.append(" \nAND ").append(joins.get(i));
                }
            }
        }

        String retString = "SELECT " + what + " \nFROM " + whereFrom + catString;
        if (!whereClause.trim().isEmpty() && !joinChecked)
            retString += " \nWHERE " + whereClause;
        if (!orderBy.trim().isEmpty())
            retString += " \nORDER BY " + orderBy;
        if (!groupBy.trim().isEmpty())
            retString += " \nGROUP BY " + groupBy;
        return retString;
    }

    private TreeView<String> generateTreeView() {
        TreeItem<String> rootItemDatabase = new TreeItem<>("Database");
        rootItemDatabase.setExpanded(true);
        TreeItem<String> rootItemTables = new TreeItem<>("Tables");
        rootItemDatabase.getChildren().add(rootItemTables);
        if (!clientForBase.tablesAvaliable()) {
            TreeItem<String> item = new TreeItem<>("Tables not available");
            rootItemTables.getChildren().add(item);
        } else {
            for (String tableName : clientForBase.getTables().getTables()) {
                TreeItem<String> tableNameItem = new TreeItem<>(tableName);
                rootItemTables.getChildren().add(tableNameItem);
                TreeItem<String> columnsItem = new TreeItem<>("Columns");
                tableNameItem.getChildren().add(columnsItem);
                for (String columnName : clientForBase.getTables().getCollumnsWithDataTypes(tableName)) {
                    TreeItem<String> columnNameItem = new TreeItem<>(columnName);
                    columnsItem.getChildren().add(columnNameItem);
                }
                TreeItem<String> primaryKeysItem = new TreeItem<>("Primary keys");
                tableNameItem.getChildren().add(primaryKeysItem);
                for (PrimaryKey primaryKey : clientForBase.getpriPrimaryKeys()) {
                    TreeItem<String> primaryKeyItem = new TreeItem<>(primaryKey.getName());
                    if (tableName.trim().equals(primaryKey.getTableName().trim())) {
                        boolean duplicateNameExists = false;
                        for (TreeItem<String> ti : primaryKeysItem.getChildren()) {
                            System.err.println(ti.getValue());
                            if (ti.getValue().trim().equals(primaryKey.getName().trim())) {
                                ti.getChildren().set(0, new TreeItem<>("Column: " + ti.getChildren().get(0).getValue() + ", " + primaryKey.getColumnName()));
                                duplicateNameExists = true;
                            }
                        }
                        if (!duplicateNameExists)
                            primaryKeysItem.getChildren().add(primaryKeyItem);
                    }
                    TreeItem<String> columnItem = new TreeItem<>("Column: " + primaryKey.getColumnName());
                    primaryKeyItem.getChildren().add(columnItem);
                }
                TreeItem<String> foreignKeysItem = new TreeItem<>("Foreign keys");
                tableNameItem.getChildren().add(foreignKeysItem);
                for (ForeignKey foreignKey : clientForBase.getForeignKeys()) {
                    TreeItem<String> foreignKeyItem = new TreeItem<>(foreignKey.getForeignKeyName());
                    if (foreignKey.getForeignTableName().trim().equals(tableName.trim())) {
                        foreignKeysItem.getChildren().add(foreignKeyItem);
                    }
                    TreeItem<String> startColumn = new TreeItem<>("Column: " + foreignKey.getForeignColumnName());
                    foreignKeyItem.getChildren().add(startColumn);
                    TreeItem<String> foreignTable = new TreeItem<>("Foreign table: " + foreignKey.getTableName());
                    foreignKeyItem.getChildren().add(foreignTable);
                    TreeItem<String> foreignColumn = new TreeItem<>("Foreign column: " + foreignKey.getColumnName());
                    foreignKeyItem.getChildren().add(foreignColumn);
                }
                TreeItem<String> uniqueKeysItem = new TreeItem<>("Unique keys");
                tableNameItem.getChildren().add(uniqueKeysItem);
                for (UniqueKey uniqueKey : clientForBase.getUniqueKeys()) {
                    TreeItem<String> uniqueKeyItem = new TreeItem<>(uniqueKey.getName());
                    if (uniqueKey.getTableName().trim().equals(tableName.trim())) {
                        uniqueKeysItem.getChildren().add(uniqueKeyItem);
                    }
                    TreeItem<String> columnUniqueItem = new TreeItem<>("Column: " + uniqueKey.getColumnName());
                    uniqueKeyItem.getChildren().add(columnUniqueItem);
                }
            }
        }
        return new TreeView<>(rootItemDatabase);
    }

    private ResultSet parseAndExecute(String text, SQLClient client, Connection conn) throws SQLException {
        String query = client.parse(text);
        Statement statement;
        ResultSet resultSet;
        statement = conn.createStatement();
        resultSet = statement.executeQuery(query);
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        for (int i = 1; i <= resultSetMetaData.getColumnCount(); ++i)
            System.out.print(resultSetMetaData.getColumnName(i) + " ");
        System.out.println("-------------------------------");
        return resultSet;
    }

    private void createAndSetSwingContent(SwingNode swingNode, JScrollPane pane) {
        SwingUtilities.invokeLater(() -> swingNode.setContent(pane));
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

    private ObservableList<String> getOptionsForDBClient() {
        return FXCollections.observableArrayList(
                "PostgreSQL",
                "SQLite",
                "Oracle DB",
                "Microsoft SQL Server"
        );
    }

    private void handleException(Throwable exception) {
        Text text = new Text(exception.getMessage());
        if (boxForQueryField.getChildren().size() > 1)
            boxForQueryField.getChildren().remove(1);
        text.setStyle("-fx-font-family: \"Arial Black\";\n" +
                "    -fx-fill:#A60F0F;\n" +
                "    -fx-font-size: 14px;");
        boxForQueryField.getChildren().add(1, text);
    }
}

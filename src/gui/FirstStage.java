package gui;

import clients.Clients;
import clients.CustomSQLClient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Properties;

public class FirstStage {
    private Stage primaryStage;

    public FirstStage(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.setTitle("Welcome");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Label database = new Label("Database:");
        grid.add(database, 0, 3);

        TextField databaseTextField = new TextField();
        grid.add(databaseTextField, 1, 3);

        Label clientLabel = new Label("SQL client:");
        grid.add(clientLabel, 0, 4);

        ComboBox clientComboBox = new ComboBox(getOptionsForDBClient());
        grid.add(clientComboBox, 1, 4);

        Button buttonForSignIn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(buttonForSignIn);
        grid.add(hbBtn, 1, 6);

        final Text actiontarget = new Text();

        Button buttonForLoadConfig = new Button("Load configuration file");
        HBox hbBtn2 = new HBox(10);
        hbBtn2.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn2.getChildren().add(buttonForLoadConfig);
        grid.add(hbBtn2, 0, 6);
        buttonForLoadConfig.setOnAction((e) -> {
            try {
                loadConfigFile(primaryStage);
            } catch (Exception e1) {
                actiontarget.setText("JSON file not found or invalid");
                actiontarget.setFill(Color.FIREBRICK);
            }
        });

        grid.add(actiontarget, 1, 7);

        Scene scene = new Scene(grid, 400, 375);

        primaryStage.setResizable(false);

        buttonForSignIn.setOnAction((e) -> {
            actiontarget.setText("");
            if (userTextField.getText().isEmpty()) {
                actiontarget.setText("Username field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (pwBox.getText().isEmpty()) {
                actiontarget.setText("Password field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (databaseTextField.getText().isEmpty()) {
                actiontarget.setText("Database field can not be empty");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            if (clientComboBox.getValue() == null) {
                actiontarget.setText("Client must be chosen");
                actiontarget.setFill(Color.FIREBRICK);
                return;
            }
            String client = (String) clientComboBox.getValue();
            try {
                startSecondView(userTextField.getText(), pwBox.getText(), databaseTextField.getText(), Clients.returnClient(client));
            } catch (SQLException e1) {
                actiontarget.setText(e1.getMessage().replace("FATAL:", ""));
                actiontarget.setFill(Color.FIREBRICK);
                System.err.println(e1.getMessage());
                primaryStage.setResizable(true);
            }
        });

        scene.setOnKeyPressed((e) -> {
            if (e.getCode() == KeyCode.ENTER) {
                buttonForSignIn.fire();
            }
        });
        scene.getStylesheets().add
                (Main.class.getResource("login.css").toExternalForm());
        primaryStage.setScene(scene);
    }

    private void loadConfigFile(Stage primaryStage) throws IOException, SQLException {
        FileChooser fileChooser = new FileChooser();
        Path path = fileChooser.showOpenDialog(primaryStage).toPath();

        Task<CustomSQLClient> parseJSONAndConnectTask = new Task<CustomSQLClient>() {
            @Override
            protected CustomSQLClient call() throws Exception {
                String connectionString = "";
                Properties props = new Properties();
                String tablesQuery = "";
                String foreignKeysQuery = "";
                boolean tablesQueryProvided = false;

                StringBuilder jsonFile = new StringBuilder();
                for (String line : Files.readAllLines(path))
                    jsonFile.append(line).append("\n");
                JSONObject jsonObject = new JSONObject(jsonFile.toString());
                JSONObject databaseSettingsObject = jsonObject.getJSONObject("databaseSettings");
                for (String key : databaseSettingsObject.keySet()) {
                    if (key.equals("connectionString"))
                        connectionString = databaseSettingsObject.getString(key);
                    if(key.equals("tablesQuery")){
                        tablesQuery = databaseSettingsObject.getString(key);
                        tablesQueryProvided = true;
                    }
                    if(key.equals("foreignKeysQuery"))
                        foreignKeysQuery = tablesQuery = databaseSettingsObject.getString(key);
                    else
                        props.setProperty(key, databaseSettingsObject.getString(key));
                }
                if(tablesQueryProvided) {
                    return new CustomSQLClient(connectionString, props, tablesQuery, foreignKeysQuery);
                }
                else
                    return new CustomSQLClient(connectionString, props);
            }
        };

        parseJSONAndConnectTask.setOnSucceeded((e) -> {
            System.out.println("Here");
            CustomSQLClient client = parseJSONAndConnectTask.getValue();
            //Design of a stage------------------------------------------------------------------------
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("SQLCompact editor");
            HBox root = new HBox();
            root.setPadding(new Insets(10));
            Button addButton = new Button("+");

            SecondStage secondStage = new SecondStage(client, secondaryStage);

            TabPane tabPane = new TabPane();
            HBox.setHgrow(tabPane, Priority.ALWAYS);

            addButton.setOnAction((event) -> {
                SecondStage secondStageNew;
                secondStageNew = new SecondStage(client, secondaryStage);
                Tab tab = createTab(secondStageNew.getBorderPane());
                tabPane.getTabs().add(tab);
                tabPane.getSelectionModel().select(tab);
            });

            tabPane.getTabs().add(createTab(secondStage.getBorderPane()));

            root.getChildren().addAll(tabPane, addButton);

            //Updating scene------------------------------------------------
            Scene scene = new Scene(root, 400, 375);
            scene.getStylesheets().add
                    (Main.class.getResource("second.css").toExternalForm());
            secondaryStage.setScene(scene);
            secondaryStage.show();
        });

        parseJSONAndConnectTask.setOnFailed((e) -> e.getSource().getException().printStackTrace());
        new Thread(parseJSONAndConnectTask).start();
    }

    private void startSecondView(String user, String pass, String database, Clients client) throws SQLException {

        //Design of a stage------------------------------------------------------------------------
        Stage secondaryStage = new Stage();
        secondaryStage.setTitle("SQL lite editor");
        HBox root = new HBox();
        root.setPadding(new Insets(10));
        Button addButton = new Button("+");

        SecondStage secondStage = new SecondStage(user, pass, database, client, secondaryStage);

        TabPane tabPane = new TabPane();
        HBox.setHgrow(tabPane, Priority.ALWAYS);

        addButton.setOnAction((event) -> {
            SecondStage secondStageNew = null;
            try {
                secondStageNew = new SecondStage(user, pass, database, client, secondaryStage);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Tab tab = createTab(secondStageNew.getBorderPane());
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(tab);
        });

        tabPane.getTabs().add(createTab(secondStage.getBorderPane()));

        root.getChildren().addAll(tabPane, addButton);

        //Updating scene------------------------------------------------
        Scene scene = new Scene(root, 400, 375);
        scene.getStylesheets().add
                (Main.class.getResource("second.css").toExternalForm());
        secondaryStage.setScene(scene);
        secondaryStage.show();

    }

    private Tab createTab(BorderPane pane) {
        Tab tab = new Tab();
        tab.setText("SQL lite editor");
        tab.setContent(pane);
        return tab;
    }

    private ObservableList<String> getOptionsForDBClient() {
        return FXCollections.observableArrayList(
                "PostgreSQL",
                "SQLite",
                "Oracle DB",
                "Microsoft SQL Server"
        );
    }

    public void show(){
        primaryStage.show();
    }
}

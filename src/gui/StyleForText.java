package gui;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.*;

public class StyleForText extends DefaultStyledDocument {

    private final StyleContext cont = StyleContext.getDefaultStyleContext();
    private final AttributeSet attrPurple = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, new Color(148, 30, 208));
    private final AttributeSet attrBlue = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, new Color(17, 42, 203));
    private final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLACK);

    public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
        super.insertString(offset, str, a);

        String text = getText(0, getLength()).toLowerCase();
        Pattern word = Pattern.compile("(\')([A-z]|\\W)*(\')");
        Matcher match = word.matcher(text);
        int before = findLastNonWordChar(text, offset);
        if (before < 0) before = 0;
        int after = findFirstNonWordChar(text, offset + str.length());
        int wordL = before;
        int wordR = before;

        while (wordR <= after) {
            if (wordR == after || String.valueOf(text.charAt(wordR)).matches("\\W")) {
                if (text.substring(wordL, wordR).matches("(\')([A-z]|\\W)*(\')"))
                    setCharacterAttributes(wordL, wordR - wordL, attrBlack, false);
                else if (text.substring(wordL, wordR).matches("(\\W)*(select|where|group|is|any|some|join)"))
                    setCharacterAttributes(wordL, wordR - wordL, attrPurple, false);
                else if (text.substring(wordL, wordR).matches("(\\W)*(from|distinct|order|or|and|not|null)"))
                    setCharacterAttributes(wordL, wordR - wordL, attrBlue, false);
                else
                    setCharacterAttributes(wordL, wordR - wordL, attrBlack, false);
                wordL = wordR;
            }
            wordR++;
        }

        int i = 0;
        while (match.find()) {
            String[] arr = getText(match.start(), match.end() - match.start()).split("\'");
            if(arr.length%2 == 1)
                continue;
            int offsetA = match.start() + 1;
            for(String stra : arr) {
                System.out.println(stra + "          " + i);
                if (i % 2 == 1)
                    setCharacterAttributes(offsetA, stra.length() + 1, attrBlack, false);
                i++;
                offsetA += stra.length() + 1;
            }
        }
    }

    public void remove(int offs, int len) throws BadLocationException {
        super.remove(offs, len);

        String text = getText(0, getLength());
        int before = findLastNonWordChar(text, offs);
        if (before < 0) before = 0;
        int after = findFirstNonWordChar(text, offs);
        if (text.substring(before, after).matches("(\')([A-z]|\\W)*(\')"))
            setCharacterAttributes(before, after - before, attrBlack, false);
        else if (text.substring(before, after).matches("(\\W)*(select|where|group|is|any|some|join)")) {
            setCharacterAttributes(before, after - before, attrPurple, false);
        } else if (text.substring(before, after).matches("(\\W)*(from|distinct|order|or|and|not|null)")) {
            setCharacterAttributes(before, after - before, attrBlue, false);
        } else {
            setCharacterAttributes(before, after - before, attrBlack, false);
        }
    }

    private int findLastNonWordChar(String text, int index) {
        while (--index >= 0) {
            if (String.valueOf(text.charAt(index)).matches("\\W")) {
                break;
            }
        }
        return index;
    }

    private int findFirstNonWordChar(String text, int index) {
        while (index < text.length()) {
            if (String.valueOf(text.charAt(index)).matches("\\W")) {
                break;
            }
            index++;
        }
        return index;
    }
}

package gui;

import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FirstStage firstStage = new FirstStage(primaryStage);
        firstStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

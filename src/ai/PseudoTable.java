package ai;

public class PseudoTable {
    private String tableName;
    private String tableAlias;

    public PseudoTable(String tableName, String tableAlias) {
        this.tableName = tableName;
        this.tableAlias = tableAlias;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PseudoTable)) return false;
        PseudoTable that = (PseudoTable) o;
        return getTableName().equals(that.getTableName()) && getTableAlias().equals(that.getTableAlias());
    }

    @Override
    public int hashCode() {
        int result = getTableName().hashCode();
        result = 31 * result + getTableAlias().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PseudoTable{" +
                "tableName='" + tableName + '\'' +
                ", tableAlias='" + tableAlias + '\'' +
                '}';
    }
}

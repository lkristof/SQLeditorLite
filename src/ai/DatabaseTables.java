package ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseTables {
    private Map<String, List<String>> tablesWithCollumns = new HashMap<>();
    private Map<String, List<String>> tablesWithColumnsAndDataTypes = new HashMap<>();

    public void addTable(String name) {
        tablesWithCollumns.put(name, new ArrayList<>()); tablesWithColumnsAndDataTypes.put(name, new ArrayList<>());
    }

    public void addCollumn(String tableName, String collumn) {
        tablesWithCollumns.get(tableName).add(collumn);
    }
    public void addCollumn(String tablename, String collumn, String dataType){
        System.out.println(dataType);
        tablesWithColumnsAndDataTypes.get(tablename).add(collumn + " (" + dataType + ")");
    }

    public List<String> getTables() {
        List<String> listForReturn = new ArrayList<>();
        for (String tableName : tablesWithCollumns.keySet())
            listForReturn.add(tableName);
        return listForReturn;
    }

    public List<String> getCollumns(String tableName) {
        return tablesWithCollumns.get(tableName);
    }
    public List<String> getCollumnsWithDataTypes(String tableName) {
        return tablesWithColumnsAndDataTypes.get(tableName);
    }

    public List<Table> getTableObjects() {
        List<Table> listForReturn = new ArrayList<>();
        for (String tableName : tablesWithCollumns.keySet()) {
            listForReturn.add(new Table(tableName, tablesWithCollumns.get(tableName)));
        }
        return listForReturn;
    }
}

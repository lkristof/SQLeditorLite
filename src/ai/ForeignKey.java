package ai;

public class ForeignKey {
    String foreignKeyName;
    String tableName;
    String columnName;
    String foreignTableName;
    String foreignColumnName;

    public ForeignKey(String foreignKeyName, String tableName, String columnName, String foreignTableName, String foreignColumnName) {
        this.foreignKeyName = foreignKeyName;
        this.tableName = tableName;
        this.columnName = columnName;
        this.foreignTableName = foreignTableName;
        this.foreignColumnName = foreignColumnName;
    }

    public String getForeignKeyName() {
        return foreignKeyName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getForeignTableName() {
        return foreignTableName;
    }

    public String getForeignColumnName() {
        return foreignColumnName;
    }
}

package ai;

public class PrimaryKey {
    String name;
    String columnName;
    String tableName;

    public PrimaryKey(String name, String columnName, String tableName) {
        this.name = name;
        this.columnName = columnName;
        this.tableName = tableName;
    }

    public String getName() {
        return name;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public String toString() {
        return "PrimaryKey{" +
                "name='" + name + '\'' +
                ", columnName='" + columnName + '\'' +
                ", tableName='" + tableName + '\'' +
                '}';
    }
}

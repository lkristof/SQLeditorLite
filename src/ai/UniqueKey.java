package ai;

public class UniqueKey {
    private String name;
    private String tableName;
    private String columnName;

    public UniqueKey(String name, String tableName, String columnName) {
        this.name = name;
        this.tableName = tableName;
        this.columnName = columnName;
    }

    public String getName() {
        return name;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniqueKey)) return false;

        UniqueKey uniqueKey = (UniqueKey) o;

        if (!getName().equals(uniqueKey.getName())) return false;
        if (!getTableName().equals(uniqueKey.getTableName())) return false;
        return getColumnName().equals(uniqueKey.getColumnName());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getTableName().hashCode();
        if(columnName != null)
            result = 31 * result + getColumnName().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UniqueKey{" +
                "name='" + name + '\'' +
                ", tableName='" + tableName + '\'' +
                ", columnName='" + columnName + '\'' +
                '}';
    }
}
